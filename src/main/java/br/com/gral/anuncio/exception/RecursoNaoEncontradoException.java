package br.com.gral.anuncio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RecursoNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private static final String message = "Recurso com id %s não encontrado.";

	public RecursoNaoEncontradoException() {
		super();
	}

	public RecursoNaoEncontradoException(String str) {
		super(String.format("%s%s", message, str));

	}

}
