package br.com.gral.anuncio.service;

import java.util.List;

import br.com.gral.anuncio.entity.Usuario;
import br.com.gral.anuncio.service.dto.UsuarioDto;

public interface UsuarioService {

    UsuarioDto login(UsuarioDto dto);

    UsuarioDto salvar(UsuarioDto dto);

	List<Usuario> getUsuarios();
}

