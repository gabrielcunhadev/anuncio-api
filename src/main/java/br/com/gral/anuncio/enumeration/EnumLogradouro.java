package br.com.gral.anuncio.enumeration;

public enum EnumLogradouro {
	
	RUA,
	AVENIDA,
	ALAMEDA,
	TRAVESSA,
	MARGINAL;

}
