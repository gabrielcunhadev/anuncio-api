package br.com.gral.anuncio.enumeration;

public enum EnumComplemento {
	
	AEROPORTO,
	ANEXO,
	ANDAR,
	BLOCO,
	CONJUNTO,
	COBERTURA,
	FRENTE,
	GALERIA,
	GRUPO,
	JUNTO,
	LOJA,
	LOTE,
	QUADRA,
	KM,
	SALA,
	SHOPPING,
	TERREO,
	VILA;

}
