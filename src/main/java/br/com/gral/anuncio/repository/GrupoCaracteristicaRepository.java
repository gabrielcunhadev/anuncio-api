package br.com.gral.anuncio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gral.anuncio.entity.GrupoCaracteristica;

@Repository
public interface GrupoCaracteristicaRepository extends JpaRepository<GrupoCaracteristica, Long> {

	List<GrupoCaracteristica> findByNomeContains(String nome);

	GrupoCaracteristica findByNome(String nome);

}
