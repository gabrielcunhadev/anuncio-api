package br.com.gral.anuncio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gral.anuncio.entity.TipoEmpreendimento;
import br.com.gral.anuncio.service.dto.TipoEmpreendimentoDto;

@Repository
public interface TipoEmpreendimentoRepository extends JpaRepository<TipoEmpreendimento, Long>{

	List<TipoEmpreendimento> findByNomeContains(String nome);
	
	TipoEmpreendimentoDto findByNome(String nome);

}
